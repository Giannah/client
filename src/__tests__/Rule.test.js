import React from 'react';
import rules from '../data.json';
import TestUtils from 'react-dom/test-utils';
import ShallowRenderer from 'react-test-renderer/shallow';
import Rule from '../Rule';

describe('Rule component', () => {
  let rule;
  let ruleElement;

  beforeEach(() => {
    rule = rules[0]
    ruleElement = <Rule 
    id = {rule.id}
    title = {rule.title}
    desc = {rule.description}
    likes = {rule.likes}
    dislikes = {rule.dislikes}
    tags = {rule.tags}
    />
  });
  
  test('should render a component Rule', () => {
    const component = TestUtils.renderIntoDocument(ruleElement);
    expect(TestUtils.isElement(ruleElement)).toBe(true);
    expect(TestUtils.isElementOfType(ruleElement,Rule)).toBe(true);
    
    const domNode = TestUtils.findRenderedDOMComponentWithClass(component,'panel panel-primary');
    expect(domNode).not.toBe(null);

    const panelHeader = TestUtils.findRenderedDOMComponentWithClass(component,'panel-heading');
    expect(panelHeader).toBeDefined();
    expect(panelHeader.textContent).toEqual(rule.title);
    
    const panelBody = TestUtils.findRenderedDOMComponentWithClass(component,'panel-body');
    expect(panelBody).toBeDefined();
    expect(panelBody.textContent).toEqual(rule.description);

    const panelFooter = TestUtils.findRenderedDOMComponentWithClass(component,'panel-footer');
    expect(panelFooter).toBeDefined();
  });

  test('should display a rule using shallow renderer', () => {
    const renderer = new ShallowRenderer();

    renderer.render(ruleElement);

    const result = renderer.getRenderOutput();

    expect(result).toBeDefined();
    expect(result.props.className).toBe('panel panel-primary');
    
    const children = result.props.children;
    
    expect(children.length).toBe(3);

    const [panelHeader, panelBody, panelFooter] = children;

    expect(panelHeader.props.className).toBe('panel-heading');
    expect(panelHeader.props.children[0]).toBe(rule.title);

    expect(panelBody.props.children).toEqual(<p>{rule.description}</p>);

    expect(panelFooter.props.className).toBe('panel-footer');
  });
});