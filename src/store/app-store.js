import { combineReducers, createStore } from 'redux';
import rulesReducer from '../reducers/rules-reducer';

const reducer = combineReducers({
  rules: rulesReducer,
});


const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
);

export default store;