import { connect } from 'react-redux';
import {doLike, doDislike} from '../actions/like-actions';
import LikeBtn from '../LikeBtn';

const mapDispatchToProps = (dispatch, OwnProps) => ({
  doLike: () => {
    dispatch(doLike(OwnProps.rules.id));
  },
  doDislike: () => {
    dispatch(doDislike(OwnProps.rules.id));
  },
})

const LikeBtnContainer = connect(
  undefined,
  mapDispatchToProps,
)(LikeBtn);

export default LikeBtnContainer;
