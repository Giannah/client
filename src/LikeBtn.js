import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

class LikeBtn extends React.Component {
  constructor(props) {
    super(props);
   
    this.increment = this.increment.bind(this);
  }

  isUp() {
    return this.props.type === 'up';
  }

  increment () {
   if(this.isUp()) {
     this.props.doLike();
   } else {
     this.props.doDislike();
   }
  }

  render () {
    const isUp = this.isUp();
    const rule = this.props;
    const counter = isUp ? rule.likes : rule.dislikes;
    const title = isUp ? '+1' : '-1';
    const classname = {
      'glyphicon-thumbs-up': isUp,
      'glyphicon-thumbs-down': !isUp,
    };
    
    return (
      <button className="btn btn-default" title={title} onClick={this.increment}>
        {counter}
        <i className={classNames('glyphicon', classname)} />
      </button>
    )
  }
}

// LikeBtn.defaultProps = {
//   counter: 0,
// }

// const LikeBtn = ({counter,title}) => {
//   const [counterUp, setCounterUp] = useState(counter);

//   const increaseCounter = () => {
//     setCounterUp(counterUp + 1);
//   }


//   const thumb = (title) => {
//     console.log('hi')
//     if(title === '+1') {
//       return 'glyphicon-thumbs-up'
//     } else if(title === '-1') {
//       return 'glyphicon-thumbs-down'
//     }
//   };

//   return (
   
//     <button className="btn btn-default" title={title} onClick={increaseCounter}>
//       {counterUp}
//       <i className={classNames('glyphicon', thumb(title))} />
//     </button>
//   )
// }

LikeBtn.propTypes = {
  type: PropTypes.oneOf(['up','down']).isRequired,
  counter: PropTypes.number,
};

export default LikeBtn;