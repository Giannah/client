export const DO_LIKE = 'DO_LIKE';
export const DO_DISLIKE = 'DO_DISLIKE';

export function doLike(id) {
  return {
    type: DO_LIKE,
    id,
  }
}

export function doDislike(id) {
  return {
    type: DO_DISLIKE,
    id,
  }
}

