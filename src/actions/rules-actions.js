import rules from '../data.json';

export const RULES_LOADED = 'RULES_LOADED';

export default function loadRules() {
  return {
    type: RULES_LOADED,
    rules,
  }
}


