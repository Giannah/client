import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import LikeBtn from './containers/LikeBtnContainer';
import './Rule.css';

class Rule extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      unfolded: props.desc,
    }
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick () {
    const { unfolded } = this.state;
    this.setState({
      unfolded: !unfolded,
    });
  }
  render() {
    const { id, title, desc, likes, dislikes, tags } = this.props;
    const { unfolded } = this.state;
    return(
      <div id={id} className="panel panel-primary"> 
          <div className="panel-heading" role="presentation" onClick={this.handleClick}>
            {title}
            <i className={classNames('pull-right','glyphicon', {'glyphicon-chevron-up':unfolded, 'glyphicon-chevron-down':!unfolded})}></i>
          </div>
          <div className={`${unfolded ? 'panel-body' : 'panelBodyHidden'}`}>
            <p>{ desc }</p>
          </div>
          <div className="panel-footer">
            <div className="btn-toolbar">
              {
                tags.map((tag) => {
                  return <span key={tag} className="badge">{tag}</span>
                })
              }
              
              <div className="btn-group btn-group-xs pull-right">
                <a href="#" className="btn btn-primary" title="Update">
                  <i className="glyphicon glyphicon-pencil"></i>
                </a>
              </div>
              <div className="btn-group btn-group-xs pull-right">
                <LikeBtn type="up" rule={likes}/>
                <LikeBtn type="down" rule={dislikes}/>
              </div>
            </div>
          </div>
        </div>
    )
  }
}

// function Rule({id, title, desc, likes, dislikes, tags}) {
  
//   const [unfolded, setUnfolded] = useState(true);

//   const handleClick = () => {
//     setUnfolded(!unfolded)
//   }
//   return (
//     <div key={id} className="panel panel-primary"> 
//     <div className="panel-heading" role="presentation" onClick={handleClick}>
//       {title}
//       <i className={classNames('pull-right','glyphicon', {'glyphicon-chevron-up':unfolded, 'glyphicon-chevron-down':!unfolded})}></i>
//     </div>
//     <div className={`${unfolded ? 'panel-body' : 'panelBodyHidden'}`}>
//       <p>{ desc }</p>
//     </div>
//     <div className="panel-footer">
//       <div className="btn-toolbar">
//         {
//           tags.map((tag) => {
//             return <span className="badge">{tag}</span>
//           })
//         }
        
//         <div className="btn-group btn-group-xs pull-right">
//           <a href="#" className="btn btn-primary" title="Update">
//             <i className="glyphicon glyphicon-pencil"></i>
//           </a>
//         </div>
//         <div className="btn-group btn-group-xs pull-right">
//           <LikeBtn type="up" title={'+1'} counter={likes} />
//           <LikeBtn type="down" title={'-1'} counter={dislikes}/>
//         </div>
//       </div>
//     </div>
//   </div>
//   )
// }

Rule.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    likes: PropTypes.number,
    dislikes: PropTypes.number,
    tags: PropTypes.arrayOf(PropTypes.string)
}

export default Rule;