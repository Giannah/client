import React from 'react';
import Rule from './Rule';

class RuleList extends React.Component {
  componentDidMount () {
    this.props.loadRules();
  }

  render() {
    const { rules } = this.props;
    console.log('rules:', rules)
    
    return (
      rules.map(rule => <Rule 
        id = {rule.id}
        title = {rule.title}
        desc = {rule.description}
        likes = {rule.likes}
        dislikes = {rule.dislikes}
        tags = {rule.tags}
      />
      ));
    }
}

// const RuleList = ({rules}) => {
//   return (
//     <main>
//       {
//         rules.map((item) => {
//           return (
//             <Rule 
//             id = {item.id}
//             title = {item.title}
//             desc = {item.description}
//             likes = {item.likes}
//             dislikes = {item.dislikes}
//             tags = {[...item.tags]}
//             />
//           )
//         })
//       }
//     </main>
//   )
// }

export default RuleList;