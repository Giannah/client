import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import { Provider } from 'react-redux';
import store from './store/app-store';
// import App from './App';
// import rules from './data';
import RuleList from './containers/RuleListContainer';
import * as serviceWorker from './serviceWorker';

// console.log('rules :', rules);

const domElement = document.getElementById('root');
const reactElement = (
  <Provider store= {store}>
    <RuleList />
  </Provider>
);
ReactDOM.render(reactElement, domElement);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
